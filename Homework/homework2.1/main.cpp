/*
* Name: Carlos Lozano
* Student ID: 2449346
* Date:
* HW:2
* Problem:1 
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;
int main()
{
    // Give and example of a syntax (compiler) error
    
    cout << "One example of a syntax error is the lack of semi-colon at the"
            << " end of a statement" << endl;
    
    cout << " cout << like this << endl" << endl;
    
    cout << "One example of a logic error is division by 0"
            << endl;
    
    cout << "10 / 0" << endl;
    
    
    return 0;
}