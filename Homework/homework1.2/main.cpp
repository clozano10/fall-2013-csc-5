/*
* Name: Carlos Lozano
* Student ID: 2449346
* Date:
* HW:1
* Problem: 
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;

int main()
{
    cout << "Hello\n";
    
    int number_of_pods, peas_per_pod, total_peas;
    int sum;
    int product;
     
    cout << "Press return after entering a number. \n";
    cout << "Enter the number of pods:\n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod:\n";
    cin >> peas_per_pod;
    total_peas = number_of_pods / peas_per_pod;
    cout << "If you have ";
    cout << number_of_pods;
    cout << " pea pods\n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods.\n";
    
    cout << "Good-bye\n" << endl;
    
    sum = number_of_pods + peas_per_pod;
    cout << "Sum of two numbers: " << sum << endl;
    
    product = number_of_pods + peas_per_pod;
    cout << "Product of two number: " <<  product << endl;
    
    
    return 0;
}

