/*
* Name: Carlos Lozano
* Student ID: 2449346
* Date:
* HW: 2
* Problem: 2
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;

int main()
{
    // part A
    cout << "******************************************\n Part A\n\n" << endl;
    
    
    cout << "The output is\n 'hello my name is Calvin\n" << "Hi there Calvin"
            << " ! My name is Bill\n Nice to meet you Bill.\n I Am wondering"
            << " if you're available for what?\n What? Bye Calvin\n Uh... sure,"
            << " bye Bill\n\n";
    
    
    // part b
    
    cout << "******************************************\n Part B\n\n" << endl;
    
    cout << "There are 10 statements in the main() function\n" << endl;
    
    // part c
    
    cout << "******************************************\n Part C\n\n" << endl;
 
    return 0;
}

